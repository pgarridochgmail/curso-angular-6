import {forwardRef, Inject, Injectable} from "@angular/core";
import {DestinoViaje} from "./destino-viaje.model";
// import {BehaviorSubject, Subject} from "rxjs";
import {ElegidoFavoritoAction, NuevoDestinoAction, VoteResetAction} from "./destinos-viajes-state.model";
import {Store} from "@ngrx/store";
import {APP_CONFIG, AppConfig, AppState, db} from "../app.module";
import {HttpClient, HttpHeaders, HttpRequest, HttpResponse} from "@angular/common/http";

@Injectable()
export class DestinosAPiClient{
  destinos: DestinoViaje[] = [];
  // public nombre:string;
  // public url:string;
  // current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);

  constructor(
    private  store: Store<AppState>,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
    private http: HttpClient
  ){
    // this.destinos = [];
    this.store
      .select(state => state.destinos)
      .subscribe((data) => {
        console.log('destinos sub store');
        console.log(data);
        this.destinos = data.items;
      });
    // this.nombre = "";
    // this.url = "";

  }

  add(d: DestinoViaje){
    // this.destinos.push(d);
    // this.store.dispatch(new NuevoDestinoAction(d));
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: headers });
    this.http.request(req).subscribe((data: HttpResponse<{}>) => {
      if (data.status === 200) {
        this.store.dispatch(new NuevoDestinoAction(d));
        const myDb = db;
        myDb.destinos.add(d);
        console.log('todos los destinos de la db!');
        myDb.destinos.toArray().then(destinos => console.log(destinos))
      }
    });
  }

  // getAll(): DestinoViaje[]{
  //   return this.destinos;
  // }

  getById(id: String): DestinoViaje{
    return this.destinos.filter(function(d){return d.id.toString() === id;})[0];
  }

  elegir(d: DestinoViaje){
    // this.destinos.forEach(x => x.setSelected(false));
    // d.setSelected(true);
    // this.current.next(d);
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }

  // subscribedOnChange(fn){
  //   // this.current.subscribe(fn);
  // }

  encerarVotos(destinos: DestinoViaje[]){
    // this.destinos.forEach(x => x.setSelected(false));
    // d.setSelected(true);
    // this.current.next(d);
    this.store.dispatch(new VoteResetAction(destinos));
  }

}
