import {v4 as uuid} from 'uuid';

export class DestinoViaje {

  selected: boolean;
  servicios: string[];
  id = uuid();

  constructor(public nombre:string,	public imagenUrl:string, public votes = 0) {
    this.servicios = ['piscina', 'desayuno'];
  }

  isSelected(): boolean {
    return this.selected;
  }

  setSelected(estado: boolean) {
    this.selected = estado;
  }

  voteUp(): any {
    this.votes++;
  }

  voteDown(): any {
    this.votes--;
  }

  voteReset(): any {
    this.votes = 0;
  }

}
