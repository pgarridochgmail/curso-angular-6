import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {DestinoViaje} from "../../models/destino-viaje.model";
import {DestinosAPiClient} from "../../models/destinos-api-client.model";
import {AppState} from "../../app.module";
import {Store} from "@ngrx/store";
// import {ElegidoFavoritoAction, NuevoDestinoAction} from "../models/destinos-viajes-state.model";

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosAPiClient]
})
export class ListaDestinosComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  trackings: String[];
  all;

  objetos: string[];
  // destinos: DestinoViaje[];


  constructor(
    private destinosApiClient: DestinosAPiClient,
    private store: Store<AppState>
  ) {
    // this.destinos = this.destinosApiClient.getAll();
    this.objetos = ['objeto 1','objeto 2','objeto 3'];
    this.onItemAdded = new EventEmitter<DestinoViaje>();
    this.updates = [];
    // this.destinosApiClient.subscribedOnChange((d: DestinoViaje) => {
    //   if (d != null) {
    //     this.updates.push('Se ha elegido a ' + d.nombre);
    //   }
    // });
  }

  ngOnInit(): void {
    this.store.select(state => state.destinos.favorito)
      .subscribe(d => {
        if (d != null) {
          this.updates.push('Se ha elegido a ' + d.nombre);
        }
      });
    this.store.select(state => state.destinos.items).subscribe(items => this.all = items);

    this.store.select(state => state.destinos.trackings)
      .subscribe(tracks => {
        if (tracks != null) {
          this.trackings = tracks;
        }
      });
  }

  // guardar(nombre:string,	url:string): boolean {
  //   console.log(this.destinos);
  //   this.destinos.push(new DestinoViaje(nombre, url));
  //   return false;
  // }

  agregado(destino: DestinoViaje) {
    this.destinosApiClient.add(destino);
    // this.destinos = this.destinosApiClient.getAll();
    this.onItemAdded.emit(destino);

  }

  elegido(destino: DestinoViaje) {
    // this.destinos.forEach(function (x) { x.setSelected(false); });
    // this.destinosApiClient.getAll().forEach(function (x) { x.setSelected(false); });
    // destino.setSelected(true);
    this.destinosApiClient.elegir(destino);
  }

  voteReset() {
    this.destinosApiClient.encerarVotos(this.all);
    return false;
  }

}
