import {Component, EventEmitter, forwardRef, Inject, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {DestinoViaje} from "../../models/destino-viaje.model";
import {fromEvent} from "rxjs";
import {debounceTime, distinctUntilChanged, filter, map, switchMap} from "rxjs/operators";
import {APP_CONFIG, AppConfig} from "../../app.module";
import {ajax} from "rxjs/ajax";

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;

  fg: FormGroup;
  minLongitud: number = 3;
  searchResults: string[] = [];
  datos: string[];

  constructor(private fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {
    this.datos = ['Lima','Cuenca','Quito','Loja','Barcelona','Buenos Aires','Barceló','Iquitos'];
    this.onItemAdded = new EventEmitter<DestinoViaje>();
    this.fg = fb.group({
      nombre: ['', [Validators.required, this.nombreValidator, this.nombreValidatorParametrizable(this.minLongitud)]],
      url: ['']
    });

    this.fg.valueChanges.subscribe(
      (form: any) => {
        console.log('form cambió:', form);
    });

    this.fg.controls['nombre'].valueChanges.subscribe(
      (value: string) => {
        console.log('nombre cambió:', value);
    });

  }

  ngOnInit(): void {
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 2),
        debounceTime(120),
        distinctUntilChanged(),
        switchMap((text: string) => {
          return ajax(this.config.apiEndpoint + '/ciudades?q=' + text)
          // this.searchResults = [];
          // return this.datos.filter(d => d.toUpperCase().includes(text.toUpperCase()));
        })
      ).subscribe(ajaxResponse =>
        // console.log(response);
        this.searchResults= ajaxResponse.response
      );
  }

  guardar(nombre: string, url: string): boolean {
    const destinoViaje = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(destinoViaje);
    return false;
  }

  nombreValidator(control: FormControl): { [s: string]: boolean } {
    const l = control.value.toString().trim().length;
    if (l > 0 && l < 4) {
      return {invalidNombre: true};
    }
    return null;
  }

  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: FormControl): { [key: string]: boolean } | null => {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < minLong) {
        return { 'minLongNombre': true };
      }
      return null;
    };
  }

}
