import {Component, OnInit, Input, HostBinding, Output, EventEmitter} from '@angular/core';
import {DestinoViaje} from "../../models/destino-viaje.model";
import {AppState} from "../../app.module";
import {Store} from "@ngrx/store";
import {EliminarDestinoAction, VoteDownAction, VoteUpAction} from "../../models/destinos-viajes-state.model";
import {animate, state, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ])
  ]
})
export class DestinoViajeComponent implements OnInit {

  @Input() destino: DestinoViaje;
  @Input('idx') posicion: number;
  @HostBinding('attr.class') cssClass='col-md-4';
  @Output() clicked: EventEmitter<DestinoViaje>;

  constructor(
    private store: Store<AppState>
  ) {
    this.clicked = new EventEmitter<DestinoViaje>();
  }

  ngOnInit(): void {
  }

  ir() {
    this.clicked.emit(this.destino);
    return false;
  }

  voteUp() {
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  voteDown() {
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }


  eliminar() {
    this.store.dispatch(new EliminarDestinoAction(this.destino));
    return false;
  }
}
