import {Component,
  Inject, Injectable, InjectionToken,
  OnInit
} from '@angular/core';
import {DestinoViaje} from "../../models/destino-viaje.model";
import {ActivatedRoute} from "@angular/router";
import {DestinosAPiClient} from "../../models/destinos-api-client.model";
import {HttpClient} from "@angular/common/http";
import {AppState} from "../../app.module";
import {Store} from "@ngrx/store";

class DestinosAPiClientViejo {
  getById(id: String): DestinoViaje {
    console.log('llamando por la clase vieja!');
    return null;
  }
}

interface AppConfig {
  apiEndpoint: String;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'http://localhost:3000'
};

const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

@Injectable()
class DestinosApiClientDecorated extends DestinosAPiClient {
  constructor(store: Store<AppState>, @Inject(APP_CONFIG) config: AppConfig, http: HttpClient ) {
    super(store, config, http);
  }
  getById(id: String): DestinoViaje {
    console.log('llamando por la clase decorada!');
    return super.getById(id);
  }
}

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [//DestinosAPiClient
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    { provide: DestinosAPiClient, useClass: DestinosApiClientDecorated },
    { provide: DestinosAPiClientViejo, useExisting: DestinosAPiClient },
  ]
  ,
  styles: [`
    mgl-map {
      height: 75vh;
      width: 75vw;
    }
  `]
})
export class DestinoDetalleComponent implements OnInit {

  destino: DestinoViaje;
  style = {
    sources: {
      world: {
        type: "geojson",
        data: "https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json"
      }
    },
    version: 8,
    layers: [{
      "id": "countries",
      "type": "fill",
      "source": "world",
      "layout": {},
      "paint": {
        'fill-color': '#6F788A'
      }
    }]
  }

  constructor(
    private route: ActivatedRoute,
    private destinosApiClient: DestinosAPiClientViejo
    // private destinosApiClient: DestinosAPiClient
  ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.destino = this.destinosApiClient.getById(id);
  }

}
