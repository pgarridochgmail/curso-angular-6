import { Component, OnInit } from '@angular/core';
import {ReservasApiClientService} from "../reservas-api-client.service";
// import { ReservasApiClientService } from '../reservas-api-client.service';

@Component({
  selector: 'app-reservas-listado',
  templateUrl: './reservas-listado.component.html',
  styleUrls: ['./reservas-listado.component.css']
})
export class ReservasListadoComponent implements OnInit {

  reservas: object[];

  constructor(private api: ReservasApiClientService ) { }

  ngOnInit() {
    this.reservas = this.api.getAll();
  }

}
