describe('ventana principal', () => {
  it('tiene encabezado correcto y en español por defecto', () => {
    cy.visit('http://localhost:4200');
    cy.contains('angular-wishlist');
    cy.get('h1 b').should('contain', 'HOLA es');
  });

  it('Muestra las coincidencias del valor digitado', () => {
    cy.visit('http://localhost:4200');
    cy.contains('angular-wishlist');
    cy.get('#nombre').type('Cuenca').should('have.value', 'Cuenca')
    cy.get('li').should('contain', 'Cuenca');
  });

  it('Llamada XHR request', () => {
    cy.request('http://localhost:3000/ciudades?q=Cuenca')
      .should((response) => {
        expect(response.status).to.eq(200)
        // the server sometimes gets an extra comment posted from another machine
        // which gets returned as 1 extra object
        expect(response.body).to.have.property('length').and.be.oneOf([1])
        expect(response).to.have.property('headers')
        expect(response).to.have.property('duration')
      })
  })
});
